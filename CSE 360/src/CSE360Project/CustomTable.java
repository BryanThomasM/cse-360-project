package CSE360Project;


import java.util.List;

//import javax.swing.event.TableModelEvent;
//import javax.swing.JList;
//import javax.swing.event.ListSelectionEvent;
//import javax.swing.event.ListSelectionListener;

/** Description of InputHandler 
 * For the Format we wanted a Custom table had to be implemented. 
 * This allowed for ease of use by user and easy readability of inputs by the systems.
 * @author Quintin Scheitlin Bryan Thomas, DickWyn, Sultan, Tyler Hagstrom
 * @version 04-07-2017
 *
 */
import javax.swing.table.AbstractTableModel;

public class CustomTable extends AbstractTableModel{
	//implements TableModelListener;
	private static final long serialVersionUID = 1L;

	private String[] columnNames = { "Line Number", "Old Spelling", "Fixed Spelling" };
	private Object[][] rowData = null;


	public CustomTable() {
		
	}

	public String getColumnName(int columnIndex) {
		return columnNames[columnIndex];
	}

	public int getRowCount() {
		return rowData.length;
	}

	public int getColumnCount() {
		return columnNames.length;
	}

	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		return rowData[rowIndex][columnIndex];
	}

	public void setValueAt(String newWord, int rowIndex, int columnIndex) {
		rowData[rowIndex][columnIndex] = newWord;
		fireTableRowsInserted(rowIndex, columnIndex);
	}
	
	public void addTable(List<String> list) {
		int index = 0;
		String[][] data2D = new String[list.size()][3];
		while (index < list.size()) {
			data2D[index][0] = ( index + 1 ) + "";
			data2D[index][1] = list.get(index).toString();
			data2D[index][2] = "  ";
			index++;
		}
		rowData = data2D;
	}
	public void deleteTable() {
		rowData = null;
	}

}
