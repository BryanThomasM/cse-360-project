package CSE360Project;


import java.util.*;
/** Description of Dictionary 
 * The Dictionary Class is comprised of three instance variables: one InputHandler object and two ArrayLists.
 * The one InputHandler object will handle input.txt and dictionary.txt and the needed persistence 
 * for the dictionary file. The ArrayList dictionary will keep track of the current words and updated words in the dictionary.
 * The ArrayList ignoreWords will keep track of the words that the user does not want to add to the dictionary file.
 * This class is a public class that supports 6 public methods and 1 public constructor. 
 * 
 * The Dictionary class represents the Dictionary. All dictionaries are implemented as instances of this class. 
 * This class includes public methods for examining the difference between the input file and 
 * the current dictionary file. In addition, it supports the ability to check if particular word exists: in the 
 * dictionary, or in the ignoreWords lists. 
 * 
 * @author Quintin Scheitlin Bryan Thomas, DickWyn, Sultan, Tyler
 * @version 04-07-2017
 *
 */
public class Dictionary {
	
		/*First InputHandler object for input file, input.txt*/
		private InputHandler fileToCompareToDictionary= null; //input object
	    
		//private InputHandler fileDictionary= null; //dictionary object
		
		/* Second InputHandler object for dictionary file, dictionary.txt*/
		private ArrayList<String> dictionary = null;
		 
		
		
		/** Description of Dictionary(InputHandler inputObject, InputHandler dictionaryObject)
		 * Initializes a new Dictionary Object so that it represents the words contained in the second argument @param dictionaryObject and compares the words in
		 * in the dictionary to the first argument @param inputObject. The constructor also initializes the ignoreWords ArrayList from null to an empty ArrayList.
		 * @param inputObject which is of type InputHandler and manages the input.txt. Instance variable fileToCompareToDictionary is an alias to this object.
		 * @param dictionaryObject which is of type InputHandler and manages the dictionary.txt. Instance variable dictionary is an alias to this object.
		 * 
		 */
		public Dictionary( InputHandler inputObject, InputHandler dictionaryObject )
		{
			this.fileToCompareToDictionary = inputObject; 
	        //this.fileDictionary = dictionaryObject;
	      
			dictionary = new ArrayList<String>(); 
	        dictionary = dictionaryObject.inputArrayList;	//array list of elements in dictionary file
			
		}
		
		
		
		
		/**
		 * Description of crossReference()
		 * Returns an ArrayList that is the difference between the words contained in the input file, but not currently in the Dictionary file. 
		 * Examples: If input.txt contains words: golf, hockey, football and the dictionary.txt contains words golf, football. 
		 * crossReference() will return an ArrayList containing one element, {hockey}
		 * @return ArrayList<String> of all words that do not exist in the dictionary file that are contained in the input file
		 * @see wordExistsInDictionary(String word)
		 * 
		 */
		
		public ArrayList <String> crossReference()
		{
			ArrayList<String> addtoDictionary = new ArrayList<String>();
			
			for( int elementPosition = 0 ; elementPosition < fileToCompareToDictionary.getSize() ; elementPosition++ )
			{
                //if the word is not already in the dictionary set addToArrayList = true
				if( !( wordExistsInDictionary( fileToCompareToDictionary.getElement( elementPosition ) ) ) )
				{		
					int newWordArrayListSize = addtoDictionary.size();
					boolean addToArrayList = true;
					
					for( int checkforDuplicates = 0 ; checkforDuplicates < newWordArrayListSize && addToArrayList; checkforDuplicates++ )
					{		
						if(addtoDictionary.get(checkforDuplicates).compareToIgnoreCase(fileToCompareToDictionary.getElement( elementPosition ) ) == 0){
                            // if duplicates exist then set addtoList = false
							addToArrayList = false;
						}
					}
					
                    //if the word is not already in the dictionary add the word
					if(addToArrayList == true){
						addtoDictionary.add( fileToCompareToDictionary.getElement( elementPosition ) );	
					}
					
				}
			}
			
			return addtoDictionary;
		}
		
		
		
		
		/**
		 * Description of wordExistsInDictionary( String word )
		 * returns true if and only if @param word is currently in the dictionary. 
		 * @param word takes a string as an argument and checks to if the word exists in the dictionary
		 * @return a boolean true if the word exists in the dictionary otherwise false
		 * @see search ( ArrayList<String> list , String target )
		 */
		public boolean wordExistsInDictionary( String word )
		{
			boolean wordExists = false;
			
			if( search( dictionary , word ) >= 0 ) 
			{
				wordExists = true;
			}
			
			return wordExists;
		}
		
		
		
		
		
		/**
		 * Description of displayAllElements()
		 * Displays to the screen a sorted and formatted list of all elements in the dictionary.
		 * Words in the dictionary are separated by a single space. 
		 */
		public void displayAllElements()
		{
			for( int elementPosition = 0 ; elementPosition < dictionary.size(); elementPosition++ )
			{
				System.out.println( dictionary.get( elementPosition ) + " " ); 
			}
			
		}
		
		
		
		
		/**
		 * Description of search (ArrayList<String> list,String target)
		 * Uses the traditional binarySearch algorithm to efficiently search a sorted list for an element in the list.
		 * search ( ArrayList<String> list , String target ) either returns the index where the @param target is located in the specified list or
		 * in the case that @param target is not contained in the list returns -1. 
		 * @param list - the list that the user wants to search in
		 * @param target - the specific word or key that the user is want to know if the word is in the list
		 * @return - returns where in the list the word exists or if it does not exists -1
		 */
		public int search ( ArrayList<String> list , String target ) {

		      int middle;
		      int from = 0;
		      int to = list.size() - 1;
		      int targetFound = -1;
		      
		      while ( from <= to && targetFound == -1 ) {
		         middle = ( from + to ) / 2;

		         if ( target.compareToIgnoreCase(list.get(middle)) == 0 )
		         {
		        	 targetFound = middle;  // target found
		         }
		         
		         if ( target.compareToIgnoreCase(list.get(middle)) > 0 )
		         {
		        	from = middle + 1;
		         }
		         else if ( target.compareToIgnoreCase(list.get(middle)) < 0 )
		         {
		            to = middle - 1;
		         }
		      }

		      return targetFound; 

		   }
		

}

