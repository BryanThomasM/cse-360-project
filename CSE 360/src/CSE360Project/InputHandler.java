package CSE360Project;

import java.io.*; 
import java.util.*; 
/** Description of InputHandler 
 * The InputHandler Class is comprised of 2 instance variables: one ArrayList object and the name of the input file.
 * The ArrayList object will handle input.txt and dictionary.txt and the needed persistence 
 * for the dictionary file. The inputfileName variable is used to open the corresponding text file.
 * This class is a public class that supports 6 public methods and 1 public constructor. 
 * 
 * The InputHandler class handles all incoming text file read and write operations.
 * This class includes public methods that read a text file and translate it into the arraylist where it can be operated upon.
 * 
 * @author Quintin Scheitlin Bryan Thomas, DickWyn, Sultan, Tyler Hagstrom
 * @version 04-07-2017
 *
 */
public class InputHandler {
	
	public ArrayList<String> inputArrayList; 
	private String inputfileName = null;
	private int numberOfLines = 0;

/** Description of InputHandler(String fileName)
 * Initializes a new InputHandler Object that reads an input file with the name @param fileName. 
 * @param fileName 
 * 
 */	
	public InputHandler( String fileName )
	{
		inputArrayList = new ArrayList<String>();
		inputfileName = fileName; 
		
	}
	
	
	
	
/** Description of loadFile()
 * Returns true only if the file with the corresponding name is opened and has its contents laoded into
 * an array list. Returns false if it fails to do so. 
 * @return a boolean true if the file is loaded properly, otherwise returns false.
 */		
	public boolean loadFile()
	{
		boolean fileOpend = false;
		
		try
		{
		
			FileReader fr = new FileReader( inputfileName );
			BufferedReader br = new BufferedReader(fr);
			Scanner inputScanner = new Scanner( new File( inputfileName ) ); //opens file
			String delims = "[ .,?!]+";
			String currentLine;       
         
         while ( ( currentLine = br.readLine() ) != null ){
        	 
        	String[] strArray = currentLine.split( delims ); 
        	numberOfLines = numberOfLines +1;
        	for(String str:strArray)
        	{
        		inputArrayList.add( str );
        	}
        	
         }
         //System.out.println(numberOfLines);
         	fr.close();
         	inputScanner.close(); //closes file
         	
         	fileOpend = true;
         
         }
		
      catch( Exception ex )
		{
			System.out.println("Reading file error");
		}
		
		return fileOpend;
	}
	
	
	
	
/** Description of displayAllElements()
 * Iterates through the arraylist and prints the contents of each element.
 * @return void.
 */			
	public void displayAllElements()
	{
		for( int allElements = 0 ; allElements < inputArrayList.size() ; allElements++ )
		{
			System.out.println( inputArrayList.get( allElements ) ); 
		}
	}
	
	
	
	
/** Description of getSize()
 * Returns the size of the array list.
 * @return an integer that is equal to the size of the array list.
 */		
	public int getSize()
	{
		return inputArrayList.size();
	}
	
	
	
	/** Description of getNumberOfLines()
	 * Returns the number of lines in the text file.
	 * @return an integer that is equal to the number of lines in the text file.
	 */	
	public int getNumberOfLines()
	{
		return numberOfLines;
	}
	
	
/** Description of getElement()
 * Searches through the array list and returns the value of the element at the position @param position.
 * @param position which is an integer that corresponds to the target index in the array list.
 * @return the value of the string in the array list at the index @param position.
 */		
	public String getElement( int position )
	{
		return inputArrayList.get( position );
	}
   
	
	
/** Description of addWord( String word )
 * Attempts to add @param word to the array list in a sorted location by iterating through the list and comparing it to the various elements. 
 * If it succeeds it returns true and returns false otherwise.
 * @param word a string representing a word to be added to the array list.
 * @return a boolean value true if it adds the word to the array list and returns false if it fails.
 */	
	
	public void addWord( String word ){
		
		boolean addingWord = false;
		
		for( int elementPosition = 0 ; elementPosition < inputArrayList.size() && !addingWord ; elementPosition++ )
		{
			String y = inputArrayList.get( elementPosition );
			int x = word.compareToIgnoreCase( y );
        
			if( x < 0 ){                          //sorts alphabetically
				inputArrayList.add( elementPosition , word );
				addingWord = true;
			}
			else if( x == 0 ){                    //duplicates
				addingWord = true;
			} 
		}
		
		if( addingWord == false )
		{
			inputArrayList.add(word);
		}
		
   }
   public void deleteWord( String word){
	   boolean deleted = false;
	   
	   
	   for(int index = 0; index< inputArrayList.size() && !deleted; index++ ){
		   String y = inputArrayList.get(index);
		   
		   if(y.compareTo(word)==0){
			   
			   inputArrayList.remove(index);
			   deleted = true;
		   }
		   
	   }
	   
	   
	   
   }
	
	
	
/** Description of updateFile()
 * This function writes the array list back into the file where it originated from in order to update
 * any changes made. 
 * @throws Exception a catch-all exception handler used to catch a file not found or write to file error.
 * @return void.
 */		
   public void updateFile(){
	   
      File fileold = new File( inputfileName );  //deletes old file
      fileold.delete();
      
      //displayAllElements();
      File filenew=new File( inputfileName ); //opens new file

      try {
    	  
         FileWriter writer = new FileWriter( filenew, false );
         
         for( int i = 0 ; i < inputArrayList.size() ; i++ )
		   {
			   writer.write(inputArrayList.get(i)+"\n");  //adds content of arraylist to new file
		   }

         writer.close();
      } 
      
      catch( Exception ex )
      {
			System.out.println("Overwriting file error");
	  } 
      
   }

   public ArrayList<String> getList(){
	   return inputArrayList;
   }
   
}