package CSE360Project;

import java.awt.EventQueue;
import java.awt.CardLayout;
import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.*;
import java.io.*;
/** Description of Driver 
 * The Driver Class links all of the other classes together to create the application.
 * It runs the Dictionary, InputHandler and CustomeTable classes. It also creates a base for the GUI. creating the 
 * Initial buttons, text areas and frames to built upon by the other classes.
 * @author Quintin Scheitlin Bryan Thomas, DickWyn, Sultan, Tyler Hagstrom
 * @version 04-07-2017
 *
 */

public class Driver {

	private JFrame frmCse;
	private String inputText = "";
	private String dectionaryText = "";
	private static InputHandler inputObject;
	private static InputHandler dictionaryObject;

	private DefaultListModel<String> listModel = new DefaultListModel<String>();
	private JList<String> list = new JList<String>(listModel);

	private CustomTable tableModel = new CustomTable();
	private JTable table = new JTable(tableModel);

	private String[] thirdCoulmnData = null;

	private static int addIndex = 0;

	private static int numberOfWordsReplaced = 0;
	private static int wordsAddedToDictionary = 0;
	private static int wordsIgnored = 0;

	private JTextField fixLineNumber;
	private JTextField fixSpelling;
	private JTextField deleteLineNumber;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {

				try {

					Driver window = new Driver();
					window.frmCse.setVisible(true);

				} catch (Exception e) {

					e.printStackTrace();

				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Driver() {

		initialize();

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		frmCse = new JFrame();
		frmCse.setTitle("CSE 360");
		frmCse.setBounds(100, 100, 474, 333);
		frmCse.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCse.getContentPane().setLayout(new CardLayout(0, 0));

		// All panels
		JPanel mainPanel = new JPanel();
		frmCse.getContentPane().add(mainPanel, "name_5816484955575");
		mainPanel.setLayout(null);
		mainPanel.setVisible(true);

		JPanel helpPanel = new JPanel();
		frmCse.getContentPane().add(helpPanel, "name_13971097528888");
		helpPanel.setLayout(null);
		helpPanel.setVisible(false);

		JPanel picWordsPanel = new JPanel();
		frmCse.getContentPane().add(picWordsPanel, "name_5840375096402");
		picWordsPanel.setLayout(null);
		picWordsPanel.setVisible(false);

		JPanel replaceWordsPanel = new JPanel();
		frmCse.getContentPane().add(replaceWordsPanel, "name_12311012390357");
		replaceWordsPanel.setLayout(null);
		replaceWordsPanel.setVisible(false);

		JPanel addIgnorePanel = new JPanel();
		frmCse.getContentPane().add(addIgnorePanel, "name_5843105611209");
		addIgnorePanel.setLayout(null);
		addIgnorePanel.setVisible(false);

		/////////////////////////////////////////////////////////////////

		String[] inputFileString = { "", "input1.txt", "input2.txt", "input3.txt" };
		String[] dictionaryInputString = { "", "dictionary1.txt", "dictionary2.txt", "dictionary3.txt" };
		String part1 = "Do you want to add ";
		String part2 = " to the ";

		// All Labels
		/// main panel Labels ///
		JLabel lblInputFile = new JLabel("Input File");
		lblInputFile.setBounds(48, 69, 160, 16);
		mainPanel.add(lblInputFile);

		JLabel lblDictionaryFile = new JLabel("Dictionary File");
		lblDictionaryFile.setBounds(272, 69, 160, 16);
		mainPanel.add(lblDictionaryFile);

		/// helpPanel Label ///
		JLabel lblHelp = new JLabel("Help:");
		lblHelp.setBounds(6, 28, 61, 16);
		helpPanel.add(lblHelp);

		/// picWordsPanel Label///
		JLabel lblPickWordToReplace = new JLabel("pick the word that you want to replace:");
		lblPickWordToReplace.setBounds(6, 27, 254, 16);
		picWordsPanel.add(lblPickWordToReplace);

		/// replaceWordsPanel Label///

		JLabel lblReplace = new JLabel("Replace ");
		lblReplace.setBounds(200, 10, 61, 16);
		replaceWordsPanel.add(lblReplace);

		JLabel fixLineNumberLbl = new JLabel("Line Number ");
		fixLineNumberLbl.setBounds(24, 174, 92, 16);
		replaceWordsPanel.add(fixLineNumberLbl);

		JLabel lblNewLabel = new JLabel("Enter the new spelling");
		lblNewLabel.setBounds(126, 174, 155, 16);
		replaceWordsPanel.add(lblNewLabel);

		JLabel deleteLineNumberLbl = new JLabel("Line Number ");
		deleteLineNumberLbl.setBounds(24, 221, 92, 16);
		replaceWordsPanel.add(deleteLineNumberLbl);

		/// addIgnorePanel Label ///
		JLabel lblAddANew = new JLabel("Add a new words to the dictionary ");
		lblAddANew.setBounds(125, 47, 219, 16);
		addIgnorePanel.add(lblAddANew);

		/// main panel JComboBox///
		JComboBox<String> inputFile = new JComboBox<String>(inputFileString);

		inputFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				inputText = (String) inputFile.getSelectedItem();
				System.out.println(inputText);

			}
		});
		inputFile.setBounds(41, 97, 167, 27);
		mainPanel.add(inputFile);

		JComboBox<String> dictionaryInput = new JComboBox<String>(dictionaryInputString);
		dictionaryInput.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				dectionaryText = (String) dictionaryInput.getSelectedItem();
				System.out.println(dectionaryText);

			}
		});
		dictionaryInput.setBounds(265, 97, 167, 27);
		mainPanel.add(dictionaryInput);

		/// helpPanel JTextPane ///
		JTextPane helpTextPane = new JTextPane();
		helpTextPane.setBounds(6, 47, 438, 225);
		helpPanel.add(helpTextPane);

		/// addIgnorePanel JTextPane ///
		JTextPane addIgnoreTextPane = new JTextPane();
		addIgnoreTextPane.setBounds(19, 75, 433, 166);
		addIgnorePanel.add(addIgnoreTextPane);

		/// replaceWordsPanel JTextField ///
		fixLineNumber = new JTextField();
		fixLineNumber.setBounds(24, 190, 83, 26);
		replaceWordsPanel.add(fixLineNumber);

		fixSpelling = new JTextField();
		fixSpelling.setBounds(126, 190, 155, 26);
		replaceWordsPanel.add(fixSpelling);

		deleteLineNumber = new JTextField();
		deleteLineNumber.setBounds(24, 238, 83, 26);
		replaceWordsPanel.add(deleteLineNumber);

		/// replaceWordsPanel JSeparator ///
		JSeparator separatorOne = new JSeparator();
		separatorOne.setBounds(6, 214, 462, 12);
		replaceWordsPanel.add(separatorOne);

		JSeparator separatorTwo = new JSeparator();
		separatorTwo.setBounds(6, 262, 462, 12);
		replaceWordsPanel.add(separatorTwo);

		///// Declaring all Button /////

		///// main panel JButton /////
		JButton btnCompare = new JButton("Compare");
		btnCompare.setBounds(159, 242, 160, 29);
		mainPanel.add(btnCompare);

		JButton btnHelp = new JButton("Help");
		btnHelp.setBounds(401, 6, 67, 37);
		mainPanel.add(btnHelp);

		///// helpPanel JButton /////
		JButton btnGoBack = new JButton("Go Back ");
		btnGoBack.setBounds(150, 6, 117, 29);
		helpPanel.add(btnGoBack);

		///// picWordsPanel JButton /////
		JButton btnCancel = new JButton("Cancel ");
		btnCancel.setBounds(70, 276, 117, 29);
		picWordsPanel.add(btnCancel);

		JButton btnReplace = new JButton("Replace ");
		btnReplace.setBounds(305, 276, 117, 29);
		picWordsPanel.add(btnReplace);

		///// replaceWordsPanel JButton /////
		JButton btnReplaceWords = new JButton("Replace ");
		btnReplaceWords.setBounds(171, 276, 117, 29);
		replaceWordsPanel.add(btnReplaceWords);

		JButton btnNewButton = new JButton("Fix");
		btnNewButton.setBounds(280, 190, 180, 29);
		replaceWordsPanel.add(btnNewButton);

		JButton btnDelete = new JButton("Delete the fixed Spelling");
		btnDelete.setBounds(280, 238, 180, 29);
		replaceWordsPanel.add(btnDelete);

		///// addIgnorePanel JButton /////
		JButton btnAdd = new JButton("Add");
		btnAdd.setBounds(85, 253, 117, 29);
		addIgnorePanel.add(btnAdd);

		JButton btnIgnore = new JButton("Ignore ");
		btnIgnore.setBounds(279, 253, 117, 29);
		addIgnorePanel.add(btnIgnore);

		/**Description of btnCompare actionPerformed(ActionEvent e) 
		 * get the file name from inputFileTextField and dictionaryFileTextField and store them as string
		 * creating two object one for the input and the second for the dictionary by using the file name
		 * load the file for both object and checking if the file is opened if not its printing the error message
		 * if both file are opened its creating a new Dictionary object to compare both file
		 * storing the different in arrayList and the size of the array list in a local variable
		 * creating array of options and asking the user to add or add all or ignore
		 * update the dictionaryObject file
		 * 
		 * after comparing the program checking for the array list size if the size is zero then it will show a message to let the 
		 * user know that all words are exist in the dictionary file else it will show a message to let the user know that the action is done
		  */		
		
		///// main panel JButton ( Compare ) /////
		btnCompare.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (inputText.equals("") || dectionaryText.equals("")) {

					JOptionPane.showMessageDialog(null, "Please choise Input file and Dictionary file");

				} else {

					inputObject = new InputHandler(inputText);
					dictionaryObject = new InputHandler(dectionaryText);
					System.out.println(inputText);
					System.out.println(dectionaryText);
					// checking if the file is opened to compare and printing

					// the error message
					boolean inputObjectFileOpen = inputObject.loadFile();
					boolean dictionaryObjectFileOpen = dictionaryObject.loadFile();

					if (!inputObjectFileOpen && !dictionaryObjectFileOpen) {

						// printing error message if both file cannot be open
						JOptionPane.showMessageDialog(null, "Input file and Dictionary file cant be open");

					} else if (!inputObjectFileOpen) {

						// printing error message if input file cannot be open
						JOptionPane.showMessageDialog(null, "Input file cant be open");

					} else if (!dictionaryObjectFileOpen) {

						// printing error message if dictionary file cannot be
						// open
						JOptionPane.showMessageDialog(null, "Dictionary file cant be open");

					} else {
						
//						Dictionary compare = new Dictionary(inputObject, dictionaryObject);
//
//						if (compare.crossReference().size() == 0) {
//							
//							JOptionPane.showMessageDialog(null, "All of the words are exist in the dictionary file");
//							
//						} else {

							mainPanel.setVisible(false);
							picWordsPanel.setVisible(true);
							
							for (int element = 0; element < inputObject.getSize(); element = element + 1) {
								listModel.addElement(inputObject.getElement(element));
								System.out.println(listModel.getElementAt(element));
							}

							list.setLayoutOrientation(JList.HORIZONTAL_WRAP);
							list.setBounds(6, 55, 462, 209);
							picWordsPanel.add(list);
						}
//					}
				}
			}
		});
		/**Description of btnHelp actionPerformed(ActionEvent e) 
		  * making the mainPanel not visible 
		  * making the helpPanel visible
		  * read the help.txt file and store the text on helpText variable 
		  * used help Text variable to set the text of the helpTextPane
		  * @exception if the help.txt file cannot be opened
		  */
		///// main panel JButton ( help ) /////
		btnHelp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// hide the main panel
				mainPanel.setVisible(false);

				// show the help panel
				helpPanel.setVisible(true);

				String helpText = "";
				try {

					FileReader fr = new FileReader("help.txt");
					BufferedReader br = new BufferedReader(fr);

					// opens the help.txt file for reading
					Scanner inputScanner = new Scanner(new File("help.txt"));

					String currentLine;

					// Storing all the word as on string in helpText
					while ((currentLine = br.readLine()) != null) {

						helpText = helpText + " " + currentLine;

					}

					// set the text helpTextPane and display it to the user
					helpTextPane.setText(helpText);

					fr.close();
					inputScanner.close(); // closes file

				}

				catch (Exception ex) {
					// System.out.println("Reading file error");
				}

			}

		});
				/**Description of btnGoBack actionPerformed(ActionEvent e) 
		  * making the mainPanel visible 
		  * making the helpPanel not visible 
		  */

		///// helpPanel JButton ( GoBack ) /////
		btnGoBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				mainPanel.setVisible(true);
				helpPanel.setVisible(false);

			}
		});

		/**Description of btnCancel actionPerformed(ActionEvent e) 
		  * making the mainPanel visible 
		  * making the picWordsPanel not visible 
		  */
		///// picWordsPanel JButton ( Cancel )/////
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				picWordsPanel.setVisible(false);
				mainPanel.setVisible(true);
				listModel.clear();
				// System.out.println(listModel.size());
			}
		});

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(24, 29, 431, 140);
		replaceWordsPanel.add(scrollPane);

			/**Description of btnReplace actionPerformed(ActionEvent e) 
		  * implemements the repalce functionality
		  */
		///// picWordsPanel JButton ( Replace )/////
		btnReplace.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println(list.getSelectedValuesList());

				System.out.println(list.getSelectedValuesList().size());
				if (list.getSelectedValuesList().size() == 0) {

					JOptionPane.showMessageDialog(null, "pick any word from the list");

				} else {

					picWordsPanel.setVisible(false);
					replaceWordsPanel.setVisible(true);

					tableModel.addTable(list.getSelectedValuesList());

					scrollPane.setViewportView(table);

				}
			}
		});
		/**Description of btnNewButton actionPerformed(ActionEvent e) 
		  * Submits the proposed repalcement
		  */
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					int integerValue = Integer.parseInt(fixLineNumber.getText());
					if (tableModel.getRowCount() < integerValue || integerValue <= 0) {
						JOptionPane.showMessageDialog(null, "Please enter a valid line number");
						fixLineNumber.setText("");
					} else {
						if (fixSpelling.getText().trim().equals("")) {
							JOptionPane.showMessageDialog(null, "Please enter a valid word");
							fixSpelling.setText("");
						} else {
							tableModel.setValueAt(fixSpelling.getText(), integerValue - 1, 2);

							fixLineNumber.setText("");
							fixSpelling.setText("");
						}
					}
				} catch (NumberFormatException ex) {
					JOptionPane.showMessageDialog(null, "Please enter a valid line number");
				}

			}
		});

				/**Description of btnDeleteactionPerformed(ActionEvent e) 
		  * Undoes the repalcement
		  */
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					int integerValue = Integer.parseInt(deleteLineNumber.getText());
					if (tableModel.getRowCount() < integerValue || integerValue <= 0) {
						JOptionPane.showMessageDialog(null, "Please enter a valid line number");
						deleteLineNumber.setText("");
					} else {
						tableModel.setValueAt("", integerValue - 1, 2);
						deleteLineNumber.setText("");

					}
				} catch (NumberFormatException ex) {
					JOptionPane.showMessageDialog(null, "Please enter a valid line number");
				}

			}
		});
				/**Description of btnNewButton actionPerformed(ActionEvent e) 
		  * Submits the proposed repalcement
		  */

		///// replaceWordsPanel JButton ( Replace ) /////
		btnReplaceWords.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				replaceWordsPanel.setVisible(false);
				addIgnorePanel.setVisible(true);

				thirdCoulmnData = new String[table.getRowCount()];

				int index = 0;
				while (index < thirdCoulmnData.length) {
					// to be changed to getValueAt(index, 2)
					String data = (String) table.getModel().getValueAt(index, 2);
					if (data.trim().equals("")) {
						thirdCoulmnData[index] = (String) table.getModel().getValueAt(index, 1);
					} else {
						inputObject.deleteWord((String) table.getModel().getValueAt(index, 1));
						inputObject.addWord(data);
						thirdCoulmnData[index] = data;
						numberOfWordsReplaced = numberOfWordsReplaced + 1;
					}
					index++;
					// System.out.println(data);
				}
				inputObject.updateFile();
				Dictionary compare = new Dictionary(inputObject, dictionaryObject);
				thirdCoulmnData = compare.crossReference().toArray(new String[compare.crossReference().size()]);
				
				addIgnoreTextPane.setText(part1 + thirdCoulmnData[addIndex] + part2);
			}
		});
		
				/**Description of btnDeleteactionPerformed(ActionEvent e) 
		  * Undoes the repalcement
		  */
		///// addIgnorePanel JButton ( Add ) /////

		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				dictionaryObject.addWord(thirdCoulmnData[addIndex]);
				wordsAddedToDictionary = wordsAddedToDictionary + 1;
				dictionaryObject.addWord(thirdCoulmnData[addIndex]);
				if (thirdCoulmnData.length > addIndex + 1) {

					addIndex = addIndex + 1;
					addIgnoreTextPane.setText(part1 + thirdCoulmnData[addIndex] + part2);

				} else if (thirdCoulmnData.length <= addIndex + 1) {
					mainPanel.setVisible(true);
					addIgnorePanel.setVisible(false);
					addIndex = 0;
					listModel.clear();
					tableModel.deleteTable();

					// addtoDictionary = null;
					JOptionPane.showMessageDialog(null, "Number of words in the input file: " + inputObject.getSize()
							+ "\nnumber of words replaced: " + numberOfWordsReplaced + "\nwords added to dictionary: "
							+ wordsAddedToDictionary + "\nnumber of lines read: " + inputObject.getNumberOfLines()
							+ "\nwords ignored: " + wordsIgnored, "CSE360 Project", JOptionPane.PLAIN_MESSAGE);
					writeStatistics();
					wordsAddedToDictionary = 0;
					wordsAddedToDictionary = 0;
					wordsIgnored = 0;
					dictionaryObject.updateFile();

				}
			}
		});
						/**Description of btnDeleteactionPerformed(ActionEvent e) 
		  * Undoes the repalcement
		  */

		///// addIgnorePanel JButton ( Ignore ) /////
		btnIgnore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				wordsIgnored = wordsIgnored + 1;
				if (thirdCoulmnData.length > addIndex + 1) {

					addIndex = addIndex + 1;
					addIgnoreTextPane.setText(part1 + thirdCoulmnData[addIndex] + part2);

				} else if (thirdCoulmnData.length <= addIndex + 1) {
					mainPanel.setVisible(true);
					addIgnorePanel.setVisible(false);
					addIndex = 0;
					listModel.clear();
					tableModel.deleteTable();

					// addtoDictionary = null;

					JOptionPane.showMessageDialog(null, "Number of words in the input file: " + inputObject.getSize()
							+ "\nnumber of words replaced: " + numberOfWordsReplaced + "\nwords added to dictionary: "
							+ wordsAddedToDictionary + "\nnumber of lines read: " + inputObject.getNumberOfLines()
							+ "\nwords ignored: " + wordsIgnored, "CSE360 Project", JOptionPane.PLAIN_MESSAGE);
					writeStatistics();
					wordsAddedToDictionary = 0;
					wordsAddedToDictionary = 0;
					wordsIgnored = 0;
					dictionaryObject.updateFile();
				}
			}
		});

	}
	public void writeStatistics(){
		
		final String NEWFILE = "Stats for " +inputText + " && " + dectionaryText;
		FileWriter fw = null;
		BufferedWriter bw = null;
		
		try {
			fw = new FileWriter(NEWFILE);
			bw = new BufferedWriter(fw);
			
			bw.write("Statistics for " + inputText + " compared against " + dectionaryText +"\n\n                      ");
			bw.write("Number of words in the input file: " + inputObject.getSize());
			bw.write("\tnumber of words replaced: " + numberOfWordsReplaced);
			bw.write("\twords added to dictionary: " + wordsAddedToDictionary);
			bw.write("\tnumber of lines read: " + inputObject.getNumberOfLines());
			bw.write("\twords ignored: " + wordsIgnored);
			System.out.println("Done Printing");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();

			}
		}
		
	}

}
